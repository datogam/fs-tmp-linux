import sys
import gdb

# Update module path.
dir_ = '/home/white/Documents/_Cateye/frida-cateyes/build/fs-linux-x86_64/share/glib-2.0/gdb'
if not dir_ in sys.path:
    sys.path.insert(0, dir_)

from gobject_gdb import register
register (gdb.current_objfile ())
